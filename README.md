# Fluent-Dark Manjaro

Kvantum theme fork based on Luwx's work (https://github.com/Luwx/Fluent-kvantum) to coincide with Manjaro's color scheme. A theme strongly inspired by Fluent Design for Kvantum.

![screenshot](https://gitlab.com/kevindeveloper/fluent-dark-manjaro/-/blob/master/Fluent-Dark-Manjaro.png)
